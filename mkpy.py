#!/usr/bin/env python
from functools import wraps
from collections import OrderedDict
import subprocess
import argparse
import shutil
import sys
import os
import errno

_tasks = OrderedDict()
_prog_args = None
_prefix_info = '*'
_prefix_warn = '?'
_prefix_err = '!'

parser = argparse.ArgumentParser(description='A mkpy based %(prog)s build file')
    
def _print(*args, **kwargs):
    if not _prog_args.quiet:
        print(*args, **kwargs)

def _info(*args, nesting=1, **kwargs):
    _print(_prefix_info*nesting, *args, **kwargs)
        
def _warn(*args, nesting=1, **kwargs):
    _print(_prefix_warn*nesting, *args, **kwargs)

def _error(*args, nesting=1, **kwargs):
    _print(_prefix_err*nesting, *args, **kwargs)

def task(*tsk_args, **tsk_kwargs):
    def decorator(fn):
        @wraps(fn)
        def wrapped(*args, **kwargs):
            if _prog_args.verbose:
                _print('')
            _info(fn.__name__)
            return fn(*args, **kwargs)
        if fn.__name__ in _tasks:
            _warn('multiple tasks with name "'+fn.__name__+'"')
        fn.argparser = argparse.ArgumentParser(*tsk_args, **tsk_kwargs)
        _tasks[fn.__name__] = wrapped
        return wrapped
    return decorator
    
def argparser(method, *ap_args, **ap_kwargs):
    def decorator(fn):
        @wraps(fn)
        def wrapped(*args, **kwargs):
            fn(*args, **kwargs)
        return wrapped
        getattr(fn.argparser, method)(*ap_args, **ap_kwargs)
    return decorator
    

def rmdirs(*dirs):
    for dir_ in dirs:
        try:
            shutil.rmtree(dir_)
        except OSError as e:
            if e.errno is errno.ENOENT:
                # Can't find it. This is okay for a rm command.
                # If you must verify existence before removal, do so explicitly.
                pass
            else:
                raise
        else:
            if _prog_args.verbose:
                _info('rmdir '+dir_, nesting=2)
        
def mkdirs(*dirs):
    for dir_ in dirs:
        try:
            os.makedirs(dir_)
        except OSError as e:
            if e.errno is errno.EEXIST:
                # Already exists. This is okay for a mk command.
                # If you must verify nonexistence before creation, do so explicitly.
                pass
            else:
                raise
        else:
            if _prog_args.verbose:
                _info('mkdir '+dir_, nesting=2)
            

def run(*args, optional=False):
    """Run command line made from args in the build process.

    - optional can be set to True if success is not required
    """
    runner = subprocess.call if optional else subprocess.check_call
    if _prog_args.verbose:
        _info(' '.join(args), nesting=2)
    try:
        runner(args, shell=False)
    except subprocess.CalledProcessError as e:
        _error('Fatal error with error code: %s' % (str(e.returncode),))
        if _prog_args.verbose and e.output is not None:
            _print('\tand output: %s' % (e.output,))
        if not optional:
            exit(e.returncode)


def build():
    """Build with command line specified task."""
    global _prog_args
    main = sys.modules['__main__']
    try:
        implementer = main.__file__
    except AttributeError:
        implementer = 'dynamically created'
    parser.add_argument('-q', '--quiet', action='store_true')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('task', nargs='?', choices=_tasks, help='Build task to run. Defaults to first task in '+implementer+', same order as this list.')
    _prog_args = parser.parse_args()
    if _prog_args.task is not None:
        _prog_args.task = _tasks[_prog_args.task]
    else:
        try:
            k, _prog_args.task = _tasks.popitem(last=False)
        except KeyError as e:
            _error('Fatal error: No tasks to run, when trying to run the default first specified task.')
            exit(1)
    _prog_args.task(args=_prog_args)
