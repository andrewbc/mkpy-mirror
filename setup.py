from distutils.core import setup

with open('README.md') as readme_f:
    long_description = readme_f.read()

kwargs = {
    'name': 'mkpy',
    'version': '0.1.0',
    'description': 'A tiny python based makefile replacement',
    'long_description': long_description,
    'author': 'AndrewBC',
    'author_email': 'andrew.b.coleman@gmail.com',
    'url': 'https://github.com/AndrewBC/mkpy',
    'download_url': 'https://github.com/AndrewBC/mkpy/downloads',
    'license': 'MIT',
    'platforms': ['Python'],
    'classifiers': [
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Build Tools'
        ],
    'py_modules': ['mkpy'],
}

setup(**kwargs)
